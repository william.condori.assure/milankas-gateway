package com.minlakas.training.milankasgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MilankasGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(MilankasGatewayApplication.class, args);
    }

}
